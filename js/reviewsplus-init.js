(function($) {
    
    $.products.reviewsplusAction = function () {
        this.load('?plugin=reviewsplus&action=moderate', function () {
            $("#s-sidebar li.selected").removeClass('selected');
            $("#s-reviewsplus").addClass('selected');
        });
    };   
    
    $.reviewsplus_moderate = {
        
        type: '',
        page: 1,
        
        init: function() {
            
            $("ul.tabs li:first", "#s-content").addClass("selected");            
            $("#reviewsplus-moderated-tab").show();
            
            $.reviewsplus_moderate.initTabs();
            $.reviewsplus_moderate.type = 'moderated';
            $.reviewsplus_moderate.showReviews(); 
            
            setTimeout(function(){
                $.reviewsplus_moderate.initCountLink();
            }, 1500);
            
        },
        
        initTabs: function() {             
            
            $("#reviewsplus-tabs").on("click", "a", function(e){                
                
                var tab = $(this).data("tab"); 
                
                $("#reviewsplus-tabs li").removeClass("selected");
                $(this).parent().addClass("selected");
                $("#s-content .tab-pane").hide();
                $("#reviewsplus-"+tab+"-tab").show(); 
                
                $.reviewsplus_moderate.type = tab;
                $.reviewsplus_moderate.page = 1;
                $.reviewsplus_moderate.showReviews();
                                
                e.preventDefault();
            });
            
            return false;            
        },
        
        showCommentDialog: function(id, review) {
            
            var comm_dialog = $("#reviewsplus-comment");
            var f = comm_dialog.find('form');
            f.trigger('reset');
            $("#reviewsplus-comment-id").val('');
            $("#reviewsplus-comment-parent-id").val('');            
            var d_title = '';
            
            if(typeof(review) !== 'undefined') {                
                
                $("#reviewsplus-comment-title").val(review[0]['title']);
                $("#reviewsplus-comment-text").val(review[0]['text']); 
                $("#reviewsplus-comment-id").val(review[0]['id']); 
                d_title = 'Ред. ответа';
            } else {     
                $("#reviewsplus-comment-parent-id").val(id);
                d_title = 'Ваш ответ';
            }
            
            var buttons = '<input type="button" class="cancel" value="Закрыть"/>';
            buttons += '&nbsp;<input type="button" class="reviewsplus-savecomment-button" value="Сохранить"/>';
            
            $("#reviewsplus-comment").waDialog({                
                width: '300px',
                height: '300px',
                title: d_title,                
                buttons: buttons
                
            });
            
            $(".reviewsplus-savecomment-button").on('click', function() {
                
                $.ajax({                    
                    url: f.attr('action'),
                    data: f.serialize(),
                    dataType: "json",
                    type: "post",
                    
                    success: function(response) {
                        
                        if(response.status !== 'fail') {
                            $.reviewsplus_moderate.showReviews();
                            $("#reviewsplus-comment").trigger('close');
                        } else {
                            if(console) {
                                console.log(response);                                
                            }
                        }
                        
                    },
                    
                    error: function(jqXHR, errorText, errorThrown) {
                        if(console){
                            console.log(jqXHR, errorText, errorThrown);
                        }
                    }
                });
                
            });
            
            return false;
            
        },
        
        showReviewEditDialog: function(id) {
            
            var buttons = '<input type="button" class="cancel" value="Закрыть"/>';
            buttons += '&nbsp;<input type="submit" class="reviewsplus-editadd-button" value="Сохранить"/>';                    
                
            $("#reviewsplus-edit").waDialog({
                width: '500px',
                buttons: buttons,
                title: 'Редактирование отзыва',
                
                onLoad: function() {
                    $(".dialog-background").on('click', function() {
                        $("#reviewsplus-edit").trigger('close');
                    });
                }
            }); 
            
            if(typeof(id) === 'undefined') {
                return false;
            }
            
            var type = $.reviewsplus_moderate.type;
            
            if(typeof(type) === 'undefined') {
                return false;
            }
            
            var f = $("#reviewsplus-edit-form");            
            
            $(".reviewsplus-editadd-button").on('click', function() {
                
                $.ajax({
                    url: f.attr('action'),
                    data: f.serialize() +
                          '&type=' + type +
                          '&id=' + id,
                    dataType: "json",
                    type: "post",
                    
                    success: function(response) {
                        
                        console.log(response);
                        
                        if(response.status !== 'fail') {
                            
                            $.reviewsplus_moderate.showReviews();
                            $("#reviewsplus-edit").trigger('close');
                        } else {
                            if(console){
                                console.log(response);
                            }
                        }                                    
                    },
                    
                    error: function(jqXHR, errorText, errorThrown) {
                        if(console){
                            console.log(jqXHR, errorText, errorThrown);
                        }
                    }
                });                
            });
            
        },
        
        initCountLink: function() {
            
            $(".reviewsplus-showcount").on("click", function() {
                
                var count = $(this).data('show-cout');
                
                if(typeof(count) === 'undefined') {
                    return false;
                }
                
                $.cookie('reviewsplus-pp', count, {expires: 30, path: '/'});
                
                $(this).closest('ul').find('li').removeClass('selected');                
                $(this).closest('li').addClass('selected');                
                    
                $.reviewsplus_moderate.page = 1;
                
                $.reviewsplus_moderate.showReviews();
                
                return false;
            });
            
            return false;
            
        },
        
        showReviews: function() {
            
            var type = $.reviewsplus_moderate.type;
            
            if(typeof(type) === 'undefined') {
                return false;
            }
            
            var page = $.reviewsplus_moderate.page;
            
            if(typeof(page) === 'undefined') {
                var page = 1;
            }
            
            $.ajax({
                
                url: '?plugin=reviewsplus&action=getReviews',
                data: 'type=' + type +
                      '&page=' + page,
                dataType: "json",
                type: "post",
                
                success: function(response) {
                    
                    if(response.status === 'fail') {
                        if(console) {
                            console.log(response);                            
                        }
                        
                        var html = '<div class="reviewsplus-no-moderated">Отзывов нет ...</div>';
                        $("#reviewsplus-" + type + "-tab .block").html(html);                        
                    } else {
                    
                        if(typeof(response.data.html) !== "undefined" && response.data.html != "") {
                            $("#reviewsplus-" + type + "-tab .block").html(response.data.html);
                        }
                    }
                },
                
                error: function(jqXHR, errorText, errorThrown) {
                    if(console){
                        console.log(jqXHR, errorText, errorThrown);
                    }
                },
                
                complete: function(jqXHR, textStatus) {                    
                    
                    if(textStatus === 'success') {
                        
                        $.reviewsplus_moderate.type = type;
                        
                        $.reviewsplus_moderate.initReviewsButtons();
                    } else {
                        if(console) {
                            console.log(jqXHR, textStatus);
                        }
                    }
                    
                }
            });       
                      
            return false;
        },             
        
        initReviewsButtons: function() {
            
            var type = $.reviewsplus_moderate.type;
            
            if(typeof(type) === 'undefined') {
                return false;
            }
            
            $(".reviewsplus-answer-button").on('click', function() {
                
                var id = $(this).closest('.reviewsplus-one-review').data('reviewsplus-review-id');
                
                if(typeof(id) !== 'undefined') {
                    $.reviewsplus_moderate.showCommentDialog(id);     
                }                       
            });
            
            $(".reviewsplus-comment-link-edit").on('click', function() {
                    
                var comm_id = $(this).closest(".reviewsplus-one-comment").data('reviewsplus-comment-id');
                
                if(typeof(comm_id) !== 'undefined') {
                    
                    $.ajax({
                        url: '?plugin=reviewsplus&action=getComment',
                        data: 'comm_id=' + comm_id,
                        dataType: "json",
                        type: "post",
                        
                        success: function(response) {
                            
                            if(response.status !== 'fail') {
                                
                                $.reviewsplus_moderate.showCommentDialog(response.data.id, response.data);
                            } else {
                                
                                if(console) {
                                    console.log(response);
                                }
                            }
                        },
                        
                        error: function(jqXHR, errorText, errorThrown) {
                            if(console){
                                console.log(jqXHR, errorText, errorThrown);
                            }
                        }
                        
                    });                    
                }                
            });
            
            $(".reviewsplus-comment-link-delete").on('click', function() {
                                    
                var comm_id = $(this).closest(".reviewsplus-one-comment").data('reviewsplus-comment-id');
                
                if(typeof(comm_id) !== 'undefined') {
                    
                    if(confirm("Удалить ответ?")) {
                        
                        $.ajax({
                            url: '?plugin=reviewsplus&action=delComment',
                            data: 'comm_id=' + comm_id,
                            dataType: "json",
                            type: "post", 
                            
                            success: function(response){
                                
                                if(response.status !== 'fail') {
                                    
                                    if(typeof(response.data.pcount) !== 'undefined') {
                                        $('.reviewsplus-published-count').html(response.data.pcount); 
                                    }
                                
                                    if(typeof(response.data.dcount) !== 'undefined') {
                                        $('.reviewsplus-deleted-count').html(response.data.dcount);                        
                                    } 
                                } else {
                                    if(console) {
                                        console.log(response);
                                    }
                                }
                               
                            },
                        
                            error: function(jqXHR, errorText, errorThrown) {
                                if(console){
                                    console.log(jqXHR, errorText, errorThrown);
                                }
                            },
                            
                            complete: function(jqXHR, textStatus) {
                                
                                if(textStatus === 'success') {
                                    $.reviewsplus_moderate.showReviews();
                                } else { 
                                    if(console) {
                                        console.log(jqXHR, textStatus);
                                    }
                                }                                
                            }
                        });                        
                    }                    
                }                    
            });
            
            $(".reviewsplus-del-button").on("click", function(){
                
                var id = $(this).closest('.reviewsplus-one-review').data('reviewsplus-review-id');
                
                if(typeof(id) !== 'undefined') {                    
                
                    if(confirm("Удалить отзыв?")) {
                        
                        var msg, count, dcount;
                        var error = false;
                        
                        $.ajax({
                            url: '?plugin=reviewsplus&action=delReview',
                            data: 'id=' + id +
                                  '&type=' + type,  
                            dataType: "json",
                            type: "post",
                    
                            success: function(response) {
                                
                                if(response.status === 'fail') {
                                    
                                    if(console) {
                                        console.log(response);
                                    }
                                    
                                    msg = response.data.errors;
                                    error = true;
                                    
                                } else {
                                    msg = response.data.msg;
                                    
                                    if(typeof(response.data.count) !== 'undefined') {
                                        $('.reviewsplus-' + type + '-count').html(response.data.count);                        
                                    }
                                    
                                    if(typeof(response.data.dcount) !== 'undefined') {
                                        $('.reviewsplus-deleted-count').html(response.data.dcount);                        
                                    }                        
                                }
                            },
                    
                            error: function(jqXHR, errorText, errorThrown) {
                                
                                if(console){
                                    console.log(jqXHR, errorText, errorThrown);
                                }
                            },
                
                            complete: function(jqXHR, textStatus) {
                                
                                if(textStatus !== 'success') {
                                    error = true;
                                    msg = 'Что-то пошло не так ...';
                                    if(console) {
                                        console.log(jqXHR, textStatus);
                                    }
                                }
                                
                                var rev_div = $(".reviewsplus-one-review[data-reviewsplus-review-id='" + id + "'");
                                var result_div = rev_div.find('.reviewsplus-result');
                                
                                $(result_div).empty(); 
                                $(result_div).html(msg);
                                
                                if(!error) {
                                    $(result_div).css('border', '1px solid green');
                                } else {
                                    $(result_div).css('border', '1px solid red');
                                }
                                
                                $(result_div).show();
                                
                                setTimeout(function(){
                                    $(result_div).empty();
                                    $(result_div).hide();
                                    $(result_div).css('border', '');
                                    
                                    if(!error) {
                                        $(rev_div).hide();
                                        $.reviewsplus_moderate.showReviews();
                                    }                                    
                                }, 1000);                    
                            }
                        });                        
                    }      
                }
                
            });
            
            
            $(".reviewsplus-edit-button").on('click', function() {                 
                
                var id = $(this).closest('.reviewsplus-one-review').data('reviewsplus-review-id');
                var type = $.reviewsplus_moderate.type; 
                
                if(typeof(id) !== 'undefined' && typeof(type) !== 'undefined') {                    
            
                    $("#reviewsplus-edit-form").trigger('reset');
            
                    $.ajax({
                        url: '?plugin=reviewsplus&action=getEditedReview',
                        data: 'id=' + id +
                              '&type=' + type,  
                        dataType: "json",
                        type: "post",
                        
                        success: function(response) {
                            
                            if(response.status !== 'fail') {
                                
                                if(typeof(response.data.review !== "undefined")) {
                        
                                    var el;
                        
                                    $.each(response.data.review, function(index, val) {
                                    
                                        el = $("#reviewsplus-edit-" + index);
                                    
                                        if(el.length) {
                                            if(index === 'rate') {
                                                $("#reviewsplus-edit-" + index + " [value=" + parseInt(val) + "]").attr("selected", "selected");
                                            } else {
                                                el.val(val);
                                            }
                                        }
                                    });
                                }
                            
                                if(typeof(response.data.dop !== "undefined")) {
                                
                                    var el;                        
                                
                                    $.each(response.data.dop, function(index, val) {
                                        el = $("#reviewsplus-edit-" + index);
                                    
                                        if(el.length) {
                                            if(index === 'rate') {                                    
                                                $("#reviewsplus-edit-" + index + " [value=" + parseInt(val) + "]").attr("selected", "selected");
                                            } else {
                                                el.val(val);
                                            }
                                        }
                                    });
                                }                                  
                            }                                           
                        },
                        
                        error: function(jqXHR, errorText, errorThrown) {
                            if(console){
                                console.log(jqXHR, errorText, errorThrown);
                            }
                        },
                    
                        complete: function(jqXHR, textStatus) {
                        
                            if(textStatus === 'success') {
                                $.reviewsplus_moderate.showReviewEditDialog(id);
                            } else {
                                if(console) {
                                    console.log(jqXHR, textStatus);
                                }
                            }
                        } 
                    });
                }                              
            });            
            
            $(".reviewsplus-return-button").on('click', function() {                
                
                var id = $(this).closest('.reviewsplus-one-review').data('reviewsplus-review-id');
                
                if(typeof(id) !== 'undefined' && type === 'deleted') {
                    
                    if(confirm("Восстановить отзыв?")) {
                        
                        $.ajax({
                            url: '?plugin=reviewsplus&action=returnDeleted',
                            data: 'id=' + id,  
                            dataType: "json",
                            type: "post",
                            
                            success: function(response) {
                                
                                if(response.status !== 'fail') {
                                    
                                    if(typeof(response.data.pcount) !== 'undefined') {
                                        
                                        $('.reviewsplus-published-count').html(response.data.pcount);                        
                                    }
                                    
                                    if(typeof(response.data.dcount) !== 'undefined') {
                                        
                                        $('.reviewsplus-deleted-count').html(response.data.dcount);                        
                                    }
                                }
                            },
                            
                            error: function(jqXHR, errorText, errorThrown) {
                                if(console){
                                    console.log(jqXHR, errorText, errorThrown);
                                }
                            },
                            
                            complete: function(jqXHR, textStatus) {
                                
                                if(textStatus === 'success') {
                                    
                                    $(".reviewsplus-one-review[data-reviewsplus-review-id='" + id + "'").hide(1000);
                                } else {
                                    if(console) {
                                        console.log(jqXHR, textStatus);
                                    }
                                }
                            }
                        });
                    }                    
                }
            });
            
            $(".reviewsplus-add-button").on('click', function() {
                
                var id = $(this).closest('.reviewsplus-one-review').data('reviewsplus-review-id');                  
                
                if(typeof(id) !== 'undefined') {
                    
                    if(confirm("Опубликовать отзыв?")) {
                        
                        var error = false;
                        var rev_div = $(".reviewsplus-one-review[data-reviewsplus-review-id='" + id + "'");
                        var result_div = rev_div.find('.reviewsplus-result');
                        $(result_div).empty();
                        var msg, count;
                        
                        $.ajax({
                            url: '?plugin=reviewsplus&action=publishModeratedReview',
                            data: 'id=' + id,
                            dataType: "json",
                            type: "post",
                            
                            success: function(response) {
                                
                                if(response.status === 'fail') {
                                    
                                    if(console) {
                                        console.log(response);
                                    }
                                    
                                    msg = response.data.errors;
                                    error = true;
                                } else {
                                    msg = response.data.msg;
                                    
                                    if(typeof(response.data.count) !== 'undefined') {
                                        $('.reviewsplus-moderated-count').html(response.data.count);    
                                    }
                    
                                    if(typeof(response.data.pcount) !== 'undefined') {
                                        $('.reviewsplus-published-count').html(response.data.pcount);                        
                                    }                     
                                }
                            },
                    
                            error: function(jqXHR, errorText, errorThrown) {
                                
                                if(console){
                                    console.log(jqXHR, errorText, errorThrown);
                                }
                            },
                
                            complete: function(jqXHR, textStatus) {
                                
                                if(textStatus !== 'success') {
                                    error = true;
                                    msg = 'Что-то пошло не так ...';
                                    if(console) {
                                        console.log(jqXHR, textStatus);
                                    }
                                }
                                
                                var rev_div = $(".reviewsplus-one-review[data-reviewsplus-review-id='" + id + "'");
                                var result_div = rev_div.find('.reviewsplus-result');
                                
                                $(result_div).empty(); 
                                $(result_div).html(msg);
                                
                                if(!error) {
                                    $(result_div).css('border', '1px solid green');
                                } else {
                                    $(result_div).css('border', '1px solid red');
                                }
                                
                                $(result_div).show();
                                
                                setTimeout(function(){
                                    $(result_div).empty();
                                    $(result_div).hide();
                                    $(result_div).css('border', '');
                                    
                                    if(!error) {
                                        $(rev_div).hide();
                                        $.reviewsplus_moderate.showReviews();
                                    }                                    
                                }, 1500);                    
                            }
                        });                        
                    }                    
                } 
            });            
            return false;            
        }        
    };    
})(jQuery);