(function($) {
    
    $.reviewsplus_settings = {
        
        template: null,
        helper_template: null,
        helper_example: null,
        helper_example2: null,
        helper_example3: null,
        all_reviews: null,
        
        init: function() {
            
            $('#reviewsplus-tabs li:first').addClass("selected");
            $("#reviewsplus-tab-content .tab-pane:first").show();           
            
            $.reviewsplus_settings.initTabs(); 
            
            $(".reviewsplus_ibutton").iButton({
                labelOn: "Вкл",
                labelOff: "Выкл",
                className: 'mini'
            }); 
            
            $(".reviewsplus_captcha").iButton({
                labelOn: "Гости",
                labelOff: "Для всех",
                className: 'mini'
            });
                        
            $("#plugins-settings-form").submit(function () {
                var f = $(this);                
                var action = f.attr('action');
                var msg = '';
                var akis_msg = '';
                var error = false;
                
                $.ajax({
                    url: action,
                    data: f.serialize(),
                    dataType: "json",
                    type: "post",
                    success: function(response) {
                        
                        if(response.status === 'fail') {
                            msg = '<i style="vertical-align:middle" class="icon16 cross"></i>' + response.errors;
                        } else {
                            msg = '<i style="vertical-align:middle" class="icon16 yes"></i>' + response.data.msg;
                        }                         
                    },
                    
                    error: function(jqXHR, errorText, errorThrown) {
                        if(console){
                            console.log(jqXHR, errorText, errorThrown);
                        }
                    },
                    
                    complete: function() {
                        $('#plugins-settings-status').html(msg);
                        $('#plugins-settings-status').show();
                        
                        setTimeout(function(){
                            $('#plugins-settings-status').hide(1500);
                            $('#plugins-settings-status').empty();                                                      
                        }, 3000);
                    }
                });
            });            
            
            $("#reviewsplus-type-selector").on('change', function() {
                
                $(".reviewsplus-coupon-type").html($(this).val());                
            });
            
            $("#reviewsplus-discount-state").on('change', function() {
                
                if($(this).is(':checked')) {
                    $("#reviewsplus-discount-div").show();
                } else {
                    $("#reviewsplus-discount-div").hide();
                }
                
            });
            
            $("#reviewsplus-akis-state").on('change', function() {
                
                if($(this).is(':checked')) {
                    $("#reviewsplus-akis-key-div").show();
                } else {
                    $("#reviewsplus-akis-key-div").hide();
                }
                
            });
            
            
            $(".reviewsplus-check-key").on('click', function() {
                
                var key = $("#reviewsplus-akis-key").val();
                
                if(typeof(key) === 'undefined') {
                    return;                    
                }
                
                var error = false;
                var msg;
                
                $.ajax({
                    url: '?plugin=reviewsplus&action=verifyAkisKey',
                    data: 'key=' + key,
                    dataType: "json",
                    type: "post",
                    
                    success: function(response) {
                        console.log(response);
                        
                        if(response.status === 'fail') {
                                                        
                            msg = response.errors;
                            error = true;
                            
                            if(console) {
                                console.log(response);
                            }                            
                        } else {
                            
                            msg = response.data.msg;                            
                        }
                        
                    },
                    
                    error: function(jqXHR, errorText, errorThrown) {
                        
                        error = true;
                        msg = 'Что-то пошло не так ...';
                        
                        if(console){
                            console.log(jqXHR, errorText, errorThrown);
                        }                        
                    },
                    
                    complete: function(jqXHR, textStatus) {
                        
                        if(textStatus !== 'success') {
                            error = true;
                            msg = 'Что-то пошло не так ...';
                            if(console) {
                                console.log(jqXHR, textStatus);
                            }
                        }
                        
                        $('.reviewsplus-akis-key-result').html(msg);
                        
                        if(!error) {
                            $('#reviewsplus-akis-key').css('border', '2px solid green');
                        } else {
                            $('#reviewsplus-akis-key').css('border', '2px solid red');
                        }
                        
                        $('.reviewsplus-akis-key-result').show();
                        
                        setTimeout(function(){
                            $('#reviewsplus-akis-key').css('border', '');
                            $('.reviewsplus-akis-key-result').hide();
                            $('.reviewsplus-akis-key-result').empty();                            
                        }, 1500);
                    }
                });
                
            });
            
            return false;            
        },
        
        initTabs: function() {             
            
            $("#reviewsplus-tabs").on("click", "a", function(e){
                
                var tab = $(this).data("tab"); 
                
                $("#reviewsplus-tabs li").removeClass("selected");
                $(this).parent().addClass("selected");
                $("#reviewsplus-tab-content .tab-pane").hide();                
                $("#reviewsplus-"+tab+"-tab").show();
                
                if(tab === 'fields') {
                    
                    $.reviewsplus_settings.buildFieldsTable();
                }
                
                
                
                if(tab === 'instruction') {
                    
                    if($.reviewsplus_settings.helper_template === null) {
                        $.reviewsplus_settings.helper_template = CodeMirror.fromTextArea(document.getElementById('reviewsplus-helper-code'), {
                            mode: "text/html",
                            tabMode: "indent",
                            height: "auto",
                            lineNumbers: true,
                            lineWrapping: true,
                            readOnly: true
                        });
                    }
                    
                    if($.reviewsplus_settings.helper_example === null) {
                        $.reviewsplus_settings.helper_example = CodeMirror.fromTextArea(document.getElementById('reviewsplus-helper-example'), {
                            mode: "text/html",
                            tabMode: "indent",
                            height: "auto",
                            lineNumbers: true,
                            lineWrapping: true,
                            readOnly: true
                        });
                    }
                    
                    if($.reviewsplus_settings.helper_example2 === null) {
                        $.reviewsplus_settings.helper_example2 = CodeMirror.fromTextArea(document.getElementById('reviewsplus-helper-example2'), {
                            mode: "text/html",
                            tabMode: "indent",
                            height: "auto",
                            lineNumbers: true,
                            lineWrapping: true,
                            readOnly: true
                        });
                    }
                    
                    if($.reviewsplus_settings.helper_example3 === null) {
                        $.reviewsplus_settings.helper_example3 = CodeMirror.fromTextArea(document.getElementById('reviewsplus-helper-example3'), {
                            mode: "text/html",
                            tabMode: "indent",
                            height: "auto",
                            lineNumbers: true,
                            lineWrapping: true,
                            readOnly: true
                        });
                    }
                }
                
                if(tab === 'template' && $.reviewsplus_settings.template === null) {
                    $.reviewsplus_settings.template = CodeMirror.fromTextArea(document.getElementById('reviewsplus-template'), {
                        mode: "text/html",
                        tabMode: "indent",
                        height: "auto",
                        lineNumbers: true,
                        lineWrapping: true                        
                    });
                    
                    $.reviewsplus_settings.initTemplateButton();
                }
                
                if(tab === 'allreviews') {
                    
                    $(".reviewsplus_ibutton_ar").iButton({
                        labelOn: "Вкл",
                        labelOff: "Выкл",
                        className: 'mini'
                    });
                    
                    if($.reviewsplus_settings.all_reviews === null) {
                        $.reviewsplus_settings.all_reviews = CodeMirror.fromTextArea(document.getElementById('reviewsplus-allreviews-template'), {
                            mode: "text/html",
                            tabMode: "indent",
                            height: "auto",
                            lineNumbers: true,
                            lineWrapping: true,                        
                            readOnly: true
                        });
                    }
                    
                    
                    $("#arsubmit-button").on('click', function () {
                        var f = $("#reviewsplus-ar-form");                
                        var action = f.attr('action');
                        var msg = '';                        
                        var error = false;
                        
                        $.ajax({
                            url: action,
                            data: f.serialize(),
                            dataType: "json",
                            type: "post",
                            
                            success: function(response) {
                                
                                if(response.status === 'fail') {
                                    msg = '<i style="vertical-align:middle" class="icon16 cross"></i>' + response.errors;
                                } else {
                                    msg = '<i style="vertical-align:middle" class="icon16 yes"></i>' + response.data.msg;
                                }                         
                            },
                            
                            error: function(jqXHR, errorText, errorThrown) {
                                if(console){
                                    console.log(jqXHR, errorText, errorThrown);
                                }
                            },
                            
                            complete: function() {
                                $('#plugins-arsettings-status').html(msg);
                                $('#plugins-arsettings-status').show();
                                
                                setTimeout(function(){
                                    $('#plugins-arsettings-status').hide(1500);
                                    $('#plugins-arsettings-status').empty();                                                      
                                }, 3000);
                            }
                        });
                    });            
                }
                
                
                e.preventDefault();
            });
            
            return false;            
        }, 
        
        initTemplateButton: function() {
            
            $("#reviewsplus-template-save").on('click', function(){
                $.reviewsplus_settings.template.save();
                
                var f = $(this).closest('form');
                var action = f.attr('action'); 
                var msg = '';
                
                $.ajax({
                    url: action,
                    data: f.serialize(),
                    dataType: "json",
                    type: "post",
                    success: function(response) {
                        
                        if(response.status == 'fail') {
                            if(console){
                                console.log(response);
                            }
                            msg = '<i class="icon16 no"></i>' + response.errors;
                            $(".reviewsplus-template-result").addClass('deliveryinfo-no');
                            
                        } else {
                        
                            msg = '<i class="icon16 yes"></i>' + response.data.msg; 
                            $(".reviewsplus-template-result").addClass('deliveryinfo-yes');
                        
                        
                            if(response.data.template) {
                                $("#reviewsplus-template-changed").hide();                            
                                $.reviewsplus_settings.template.setValue(response.data.template);
                            }
                        
                            if(response.data.chg) {
                                $("#reviewsplus-template-changed").show();
                            } 
                        }
                        
                    },
                    error: function(jqXHR, errorText, errorThrown) {
                        if(console){
                            console.log(jqXHR, errorText, errorThrown);
                        }
                    },
                    complete: function() {  
                        $("input[name=reviewsplus-template-reset]").removeAttr('checked');
                        $(".reviewsplus-template-result").html(msg);
                        $(".reviewsplus-template-result").show();
                        setTimeout(function(){
                            $(".reviewsplus-template-result").hide(1500);
                            $(".reviewsplus-template-result").empty();
                            $(".reviewsplus-template-result").removeClass('deliveryinfo-yes');
                            $(".reviewsplus-template-result").removeClass('deliveryinfo-no');
                        }, 3000);                        
                    }                    
                });
            });
        },
        
        initFieldsButton: function() {
            
            $('.reviewsplus-field-delete').on('click', function() {
                
                if(!confirm("Удалить поле? Внимание!!! Все данные отзывов для этого поля тоже будут удалены!!!")) {
                    return false;
                }
                
                var tr = $(this).closest('tr');
                var field_id = tr.data('reviewsplus-fields-id');
                
                if(typeof(field_id) !== "undefined" && field_id != "") {
                    
                    $.ajax({
                        url: '?plugin=reviewsplus&action=delReviewsField',
                        data: 'field_id=' + field_id,
                        dataType: "json",
                        type: "post",
                        
                        success: function(response) {
                            
                            if(response.status === 'fail') {
                                if(console){
                                    console.log(response.errors);
                                }
                                return false;
                            }
                            
                            tr.hide(1000);
                            
                            setTimeout(function(){                                    
                                tr.remove();
                            }, 1500);                                                                                         
                            
                        },
                        
                        error: function(jqXHR, errorText, errorThrown) {
                            if(console){
                                console.log(jqXHR, errorText, errorThrown);
                            }
                        }                        
                    });                    
                }
                
                return false;
                
            });
            
            $('.reviewsplus-field-edit').on('click', function() {
                var tr = $(this).closest('tr');
                var field_id = tr.data('reviewsplus-fields-id');
                
                if(typeof(field_id) !== "undefined" && field_id != "") {
                    
                    $.ajax({
                        url: '?plugin=reviewsplus&action=editReviewsField',
                        data: 'field_id=' + field_id,
                        dataType: "json",
                        type: "post",
                        
                        success: function(response) {
                            
                            if(response.status === 'fail') {
                                if(console){
                                    console.log(response);
                                }
                                return false;
                            }
                            
                            $.reviewsplus_settings.showFieldDialog(response.data.field);
                            
                        },
                        
                        error: function(jqXHR, errorText, errorThrown) {
                            if(console){
                                console.log(jqXHR, errorText, errorThrown);
                            }
                        }                        
                    });                    
                }
                
                return false;
            });
            
            $('.reviewsplus-field-add').on('click', function() {
                $.reviewsplus_settings.showFieldDialog();
            });
        },
        
        showFieldDialog: function(field) {
                        
            var dialog = $("#reviewsplus-dialog");
            var f = dialog.find('form');            
            var action = f.attr('action');
            
            var id;            
            
            if(typeof(field) !== "undefined") {
                
                dialog.find('h2').html('Редактирование поля');    
                id = field.id;  
                
                $("#reviewsplus-field-select-type").attr('disabled', 'disabled');
                $("#reviewsplus-field-name").val(field.name);
                $("#reviewsplus-field-select-req").removeAttr('disabled');                
                            
                $("#reviewsplus-field-select-type [value=" + field.type + "]").attr("selected", "selected");
                
                if(field.type == 'rate') {
                    $("#reviewsplus-field-select-req [value=0]").attr("selected", "selected");
                    $("#reviewsplus-field-select-req").attr('disabled', 'disabled');
                } else {
                    $("#reviewsplus-field-select-req [value=" + field.required + "]").attr("selected", "selected");
                }             
                
                $("#reviewsplus-field-select-show [value=" + field.show + "]").attr("selected", "selected");
                                
                $("#reviewsplus-field-sort").val(field.sort);
                
                
            } else {
                dialog.find('h2').html('Добавление нового поля');
                id = 0;
                
                $("#reviewsplus-field-select-req").removeAttr('disabled'); 
                $("#reviewsplus-field-select-type").removeAttr('disabled');
                $("#reviewsplus-field-name").val('');
                $("#reviewsplus-field-select-req [value=1]").attr("selected", "selected");                
                $("#reviewsplus-field-select-type [value=text]").attr("selected", "selected");
                
                $("#reviewsplus-field-sort").val("0");
            }
            
            var buttons = '<input type="button" class="cancel" value="Закрыть"/>';
            buttons += '&nbsp;&nbsp<input type="button" value="Сохранить" class="reviewsplus-field-save" />';
            
            dialog.waDialog({
                'width': '300px',
                'height': '360px',
                'buttons': buttons
            });
            
            $("#reviewsplus-field-select-type").on('change', function() {
               if($(this).val() == "rate") {
                   $("#reviewsplus-field-select-req [value=0]").attr("selected", "selected");
                   $("#reviewsplus-field-select-req").attr('disabled', 'disabled');
               } else {
                   $("#reviewsplus-field-select-req").removeAttr('disabled');
               }
            });
            
            
            $(".reviewsplus-field-save").on('click', function(){
                
                if($("#reviewsplus-field-name").val() == "") {
                    
                    $('.reviewsplus-dialog-result').html('Не задано название');
                    $("#reviewsplus-field-name").css('border', '2px dotted red');
                    $('.reviewsplus-dialog-result').addClass('reviewsplus-no');
                    $('.reviewsplus-dialog-result').show();
                    
                    setTimeout(function(){
                        $("#reviewsplus-field-name").css('border', '');
                        $('.reviewsplus-dialog-result').hide();
                        $('.reviewsplus-dialog-result').empty();
                        $('.reviewsplus-dialog-result').removeClass('reviewsplus-no');
                    }, 1500);
                    
                    return false;
                }  
                
                var msg = '';
                var error = false;
                
                $.ajax({
                    url: action,
                    data: f.serialize() +
                          '&id=' + id,
                    dataType: "json",
                    type: "post",
                    
                    success: function(response) {
                        
                        if(response.status === 'fail') {
                            
                            if(console){
                                console.log(response);
                            }
                            
                            if(typeof(response.errors) !== 'undefined') {
                                msg = response.errors;
                            }  
                            
                            error = true;
                            
                        } else {
                            
                            if(typeof(response.data.msg) !== 'undefined') {
                                msg = response.data.msg
                            }                            
                        }
                            
                    },
                        
                    error: function(jqXHR, errorText, errorThrown) {
                        if(console){
                            console.log(jqXHR, errorText, errorThrown);
                        }
                    }, 
                    
                    complete: function(jqXHR, textStatus) {
                        
                        if(textStatus !== 'success') {
                            error = true;
                            msg = 'Что-то пошло не так ...';
                            if(console) {
                                console.log(jqXHR, textStatus);
                            }
                        }
                        
                        $('.reviewsplus-dialog-result').html(msg);
                        
                        if(!error) {
                            $('.reviewsplus-dialog-result').addClass('reviewsplus-yes');
                            $.reviewsplus_settings.buildFieldsTable();
                        } else {
                            $('.reviewsplus-dialog-result').addClass('reviewsplus-no');
                        }
                        
                        $('.reviewsplus-dialog-result').show();
                        
                        setTimeout(function(){
                            $('.reviewsplus-dialog-result').removeClass('reviewsplus-no');
                            $('.reviewsplus-dialog-result').removeClass('reviewsplus-yes');
                            $('.reviewsplus-dialog-result').hide();
                            $('.reviewsplus-dialog-result').empty();
                            
                            if(!error) {
                                dialog.trigger('close');
                            }
                            
                        }, 1500);
                    }
                });                
                return false;
            });
            
        },
        
        buildFieldsTable: function() {
            
            var tbody = $("#reviewsplus-tbody");
            
            $.ajax({
                url: '?plugin=reviewsplus&action=buildFieldsTable',
                dataType: "json",
                type: "post",
                
                success: function(response) {
                    
                    
                    if(response.errors) {
                        if(console){
                            console.log(response.errors);
                        }
                        return false;
                    }
                    
                    tbody.html(response.data.html);
                    $.reviewsplus_settings.initFieldsButton(); 
                },
                
                error: function(jqXHR, errorText, errorThrown) {
                    if(console){
                        console.log(jqXHR, errorText, errorThrown);
                    }
                }
                
            });
            
            return false;
        }
    };
    
})(jQuery);