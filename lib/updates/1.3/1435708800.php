<?php

$plugin_id = array('shop', 'reviewsplus');
$app_settings_model = new waAppSettingsModel(); 

$app_settings_model->set($plugin_id, 'discount_state', '0');
$app_settings_model->set($plugin_id, 'discount_text', '1');

$cur = wa()->getConfig()->getAppConfig('shop')->getOption('currency');

$coupon = array('type' => $cur, 'value' => 0, 'days' => 0);

$app_settings_model->set($plugin_id, 'coupon', json_encode($coupon));
