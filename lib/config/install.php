<?php

$plugin_id = array('shop', 'reviewsplus');
$app_settings_model = new waAppSettingsModel(); 
//Изначально плагин выключен
$app_settings_model->set($plugin_id, 'state', '0');

$app_settings_model->set($plugin_id, 'reviews_per_page', '0');
$app_settings_model->set($plugin_id, 'reviews_answers', '0');
$app_settings_model->set($plugin_id, 'reviews_sort', 'date');
$app_settings_model->set($plugin_id, 'add_popup', '0');
$app_settings_model->set($plugin_id, 'moderate', '0');
$app_settings_model->set($plugin_id, 'captcha', '0');
$app_settings_model->set($plugin_id, 'email', '');
$app_settings_model->set($plugin_id, 'akis_state', '0');
$app_settings_model->set($plugin_id, 'akis_key', '');
$app_settings_model->set($plugin_id, 'discount_state', '0');
$app_settings_model->set($plugin_id, 'discount_text', '1');
$app_settings_model->set($plugin_id, 'noreviews', '');

$app_settings_model->set($plugin_id, 'arstate', '0');
$app_settings_model->set($plugin_id, 'ar_reviews_per_page', '0');
$app_settings_model->set($plugin_id, 'ar_reviews_answers', '0');


$cur = wa()->getConfig()->getAppConfig('shop')->getOption('currency');

$coupon = array('type' => $cur, 'value' => 0, 'days' => 0);

$app_settings_model->set($plugin_id, 'coupon', json_encode($coupon));

$fields_model = new shopReviewsplusPluginFieldsModel();

$data = array();

$data[] = array('name' => 'Заголовок',
                'name_id' => 'title',
                'required' => 0,
                'delete' => 0,                
                'type' => 'text',
                'func' => 'main',
                'sort' => 2,
               );

$data[] = array('name' => 'Сайт',
                'name_id' => 'site',
                'required' => 0,
                'delete' => 0,                
                'type' => 'text',
                'func' => 'main',
                'sort' => 3,
               );

$data[] = array('name' => 'Общий рейтинг',
                'name_id' => 'rate',
                'required' => 1,
                'delete' => 0,                
                'type' => 'rate',
                'func' => 'main',
                'sort' => 1,
               );

$data[] = array('name' => 'Имя',
                'name_id' => 'name',
                'required' => 1,
                'delete' => 0,                
                'type' => 'text',
                'func' => 'main',
                'sort' => 0,
               );

$data[] = array('name' => 'Комментарий',
                'name_id' => 'text',
                'required' => 1,
                'delete' => 0,                
                'type' => 'textarea',
                'func' => 'main',
                'sort' => 4,
               );

$data[] = array('name' => 'Email',
                'name_id' => 'email',
                'required' => 0,
                'delete' => 0,                
                'type' => 'text',
                'func' => 'main',
                'sort' => 4,
               );

if($fields_model->countAll() == 0) {
    $fields_model->multipleInsert($data);
}







