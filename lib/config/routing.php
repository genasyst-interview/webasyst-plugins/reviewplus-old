<?php

return array(
    'reviewsplus/addreview' => 'frontend/addReview',
    'reviewsplus/getform' => 'frontend/getForm',
    'reviewsplus/delreview' => 'frontend/delReview',
    'reviewsplus/showreviewspage' => 'frontend/showReviewsPage',
    'reviewsplus/allreviews'  => 'frontend/getAllReviews',
);
