<?php

return array(
    'shop_reviewsplus_fields' => array(        
        'id' => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'name_id' => array('varchar', 50, 'null' => 0),
        'name' => array('varchar', 50, 'null' => 0,),
        'required' => array('tinyint', 1, 'null' => 0, 'default' => '1'),        
        'delete' => array('tinyint', 1, 'null' => 0, 'default' => '1'),
        'show' => array('tinyint', 1, 'null' => 0, 'default' => '1'),
        'type' => array('enum', "'rate','text','textarea'", 'null' => 0, 'default' => 'rate'),
        'func' => array('enum', "'main','dop'", 'null' => 0, 'default' => 'dop'),
        'sort' => array('tinyint', 3, 'null' => 0, 'default' => '0'),        
        ':keys' => array(
            'PRIMARY' => array('id'),
            'INDEX' => array('name_id'),
        ),
    ),
    
    'shop_reviewsplus_dop' => array(        
        'id' => array('int', 11, 'null' => 0, 'autoincrement' => 1),  
        'review_id' => array('int', 11, 'null' => 0, 'default' => 0),
        'moderate_id' => array('int', 11, 'null' => 0, 'default' => 0),
        ':keys' => array(
            'PRIMARY' => array('id'),
            'INDEX' => array('review_id', 'moderate_id'),
        ),
    ),
    
    'shop_reviewsplus_moderated' => array(        
        'id' => array('int', 11, 'null' => 0, 'autoincrement' => 1),  
        'auth_provider' => array('enum', "'user', 'guest'", 'null' => 0, 'default' => 'guest'),
        'contact_id' => array('int', 11, 'default' => NULL),
        'name' => array('varchar', 50, 'default' => NULL),
        'email' => array('varchar', 50, 'default' => NULL),
        'product_id' => array('int', 11, 'null' => 0, 'default' => NULL),
        'rate' => array('decimal', "3,2", 'default' => 0.0),
        'site' => array('varchar', 100, 'default' => NULL),
        'text' => array('text', 'default' => NULL),
        'title' => array('varchar', 255, 'default' => NULL),
        'datetime' => array('datetime'),
        'ip' => array('int', 11, 'default' => NULL),
        'spam' => array('tinyint', 2, 'default' => 0),
        ':keys' => array(
            'PRIMARY' => array('id'),            
        ),
    ),
);
