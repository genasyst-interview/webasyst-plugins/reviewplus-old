 <?php

return array(
    'name'        => 'Reviews Plus',
    'description' => 'Расширенные отзывы',
    'img'        => 'img/icon.png',
    'vendor'      => '986052',
    'version'     => '2.3',
    'shop_settings' => true,
    'frontend' => true,    
    'handlers'    => array('backend_products' => 'backendProducts',
                           'frontend_header' => 'frontendHeader',
                          )
);

//EOF
