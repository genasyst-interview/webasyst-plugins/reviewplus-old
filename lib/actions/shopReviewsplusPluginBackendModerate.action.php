<?php
/** 
 * @desc
 * Экшен для бекенда
 * Вкладка "Расширенные отзывы"
 * 
 * @author мастерская BNP <support@byloneprosto.ru> 
 */

class shopReviewsplusPluginBackendModerateAction extends waViewAction
{    
    public function execute() {
        
        $moderate_model = new shopReviewsplusPluginModeratedModel();
        $moderate_count = $moderate_model->countAll();
        $this->view->assign('moderate_count', $moderate_count);
        
        $reviews_model = new shopProductReviewsModel();
        
        $pcount = $reviews_model->countByField(array('status' => 'approved', 'depth' => 0));
        $this->view->assign('published_count', $pcount);
        
        $dcount = $reviews_model->countByField('status', 'deleted');
        $this->view->assign('deleted_count', $dcount);        
        
        $form_fields = shopReviewsplusPlugin::getFieldsAsType();
        
        if(isset($form_fields['rate'])) {
            $this->view->assign('rate_fields', $form_fields['rate']);
        }
        
        if(isset($form_fields['text'])) {
            $this->view->assign('text_fields', $form_fields['text']);
        }
        
        if(isset($form_fields['textarea'])) {
            $this->view->assign('textarea_fields', $form_fields['textarea']);
        }
        
        $limit = waRequest::cookie('reviewsplus-pp', 5, 'int');
        
        $this->view->assign('limit', $limit);
       
    }
}