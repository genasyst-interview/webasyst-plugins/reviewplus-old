<?php

class shopReviewsplusPluginSettingsAction extends waViewAction
{
    public function execute() {
        
        $js_path = shopReviewsplusPlugin::getPluginPath('js');
        $plugin_id = array('shop', 'reviewsplus');
        $this->view->assign('js_path', $js_path);
        
        $app_settings_model = new waAppSettingsModel();        
        $settings = $app_settings_model->get($plugin_id);
        $this->view->assign('settings', $settings);
        
        $temp = shopReviewsplusPlugin::getPluginPath('templates', 'frontend/reviews-list.html');
        $template = file_get_contents($temp['path']);
        $this->view->assign('template', $template);
        $this->view->assign('tml_change', $temp['changed']);
        
        $tpath = shopReviewsplusPlugin::getPluginPath('templates');
        $all_reviews = file_get_contents($tpath['path'].'example/all-reviews.html');
        $this->view->assign('all_reviews', $all_reviews);
        
        $config = wa()->getConfig();
        
        $this->view->assign(array(
            'request_captcha' => $config->getGeneralSettings('require_captcha'),
            'require_authorization' => $config->getGeneralSettings('require_authorization')
        ));
                
        if(isset($settings['coupon'])) {
            $coupon = json_decode($settings['coupon'], true);
            $this->view->assign('coupon', $coupon);
        }
    } 
    
}
