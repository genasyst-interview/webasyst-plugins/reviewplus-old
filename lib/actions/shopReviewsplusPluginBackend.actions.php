<?php
/**
 * shopReviewsplusPluginBackenActions
 * @desc
 * Обработка экшенов бекенда
 * для плагина ReviewsPlus
 * 
 * @author мастерская BNP <support@byloneprosto.ru> 
 */

class shopReviewsplusPluginBackendActions extends waJsonActions
{       
    public function __construct() {
        $this->fields_model = new shopReviewsplusPluginFieldsModel();
        $this->dop_model = new shopReviewsplusPluginDopModel();
        $this->wa_model = new waModel();
        $this->moderate_model = new shopReviewsplusPluginModeratedModel();
        $this->reviews_model = new shopProductReviewsModel();
        
        $this->plugin_id = array('shop', 'reviewsplus');
        $this->app_settings_model = new waAppSettingsModel();
    }
    
    /**
     * Запись настроек
     */
    public function saveSettingsAction() {
        
        $settings = waRequest::post('settings');
        
        $coupon = waRequest::post('coupon', array());
        
        if(isset($settings['discount_state'])){
            if(!isset($coupon['value']) || empty($coupon['value'])) {
                $this->errors = 'Скидка по купону не может быть пустой или нулем';
            } else {
                $coupon['value'] = (float) str_replace(',', '.', $coupon['value']);
                if(!is_numeric($coupon['value'])) {
                    $this->errors = 'Скидка по купону должна быть числом';
                }                
            }            
        }        
        
        if(!empty($this->errors)) {
            return;
        }
        
        if(!isset($coupon['days']) || empty($coupon['days'])) {
            $coupon['days'] = 0;            
        }
        
        if ($coupon['type'] == '%') {
            $coupon['value'] = min(max($coupon['value'], 0), 100);            
        }
        
        if(!isset($coupon) || empty($coupon)) {
            $cur = wa()->getConfig()->getAppConfig('shop')->getOption('currency');
            $coupon = array('type' => $cur, 'value' => 0, 'days' => 0);            
        } 
        
        $this->app_settings_model->set($this->plugin_id, 'coupon', json_encode($coupon));
        
        if(isset($settings['discount_text'])){
            $this->app_settings_model->set($this->plugin_id, 'discount_text', 1);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'discount_text', 0);
        } 
        
        //Включение/выключение
        if(isset($settings['state'])){
            $this->app_settings_model->set($this->plugin_id, 'state', 1);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'state', 0);
        } 
        
        if(isset($settings['captcha'])){
            $this->app_settings_model->set($this->plugin_id, 'captcha', 1);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'captcha', 0);
        } 
        
        if(isset($settings['reviews_per_page'])){
            $this->app_settings_model->set($this->plugin_id, 'reviews_per_page', (int)$settings['reviews_per_page']);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'reviews_per_page', 0);
        }
        
        if(isset($settings['reviews_answers'])){
            $this->app_settings_model->set($this->plugin_id, 'reviews_answers', 1);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'reviews_answers', 0);
        }
        
        if(isset($settings['reviews_sort']) && !empty($settings['reviews_sort'])){
            $this->app_settings_model->set($this->plugin_id, 'reviews_sort', $settings['reviews_sort']);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'reviews_sort', 'datetime');
        }
        
        if(isset($settings['add_popup'])){
            $this->app_settings_model->set($this->plugin_id, 'add_popup', 1);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'add_popup', 0);
        } 
        
        if(isset($settings['moderate'])){
            $this->app_settings_model->set($this->plugin_id, 'moderate', 1);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'moderate', 0);
        } 
        
        if(isset($settings['email']) && !empty($settings['email'])){
            $this->app_settings_model->set($this->plugin_id, 'email', $settings['email']);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'email', '');
        }
        
        if(isset($settings['noreviews'])){
            $this->app_settings_model->set($this->plugin_id, 'noreviews', $settings['noreviews']);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'noreviews', '');
        } 
        
        if(isset($settings['discount_state'])){
            $this->app_settings_model->set($this->plugin_id, 'discount_state', 1);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'discount_state', 0);
        }
        
        $shop = waRequest::post('shop', array());
        
        if(isset($shop['authorization'])) {
            $this->app_settings_model->set('shop', 'require_authorization', 1);
        } else {
            $this->app_settings_model->set('shop', 'require_authorization', 0);
        }
        
        if(isset($shop['captcha'])) {
            $this->app_settings_model->set('shop', 'require_captcha', 1);
        } else {
            $this->app_settings_model->set('shop', 'require_captcha', 0);
        }
        
        
        if(isset($shop['coupon'])) {
            $this->app_settings_model->set('shop', 'discount_coupons', 1);            
        } else {
            $this->app_settings_model->set('shop', 'discount_coupons', 0);            
        }
        
        if(isset($settings['akis_state'])){
            $this->app_settings_model->set($this->plugin_id, 'akis_state', 1);            
        } else {
            $this->app_settings_model->set($this->plugin_id, 'akis_state', 0);
        } 
        
        if(isset($settings['akis_key']) && !empty($settings['akis_key'])){
            $this->app_settings_model->set($this->plugin_id, 'akis_key', $settings['akis_key']);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'akis_key', '');
        }
                
        $this->response = array('msg' => 'Сохранено');
        
    } 
    
    public function saveTemplateAction() {
        
        $path = 'plugins/reviewsplus/templates/frontend/reviews-list.html';
        
        $reset = waRequest::post('reviewsplus-template-reset', 0, 'int'); 
        
        if($reset !== 0) {
            $template_path = wa()->getDataPath($path, false, 'shop', true);
            waFiles::delete($template_path);
            $template_path = wa()->getAppPath($path, 'shop');
            $template = file_get_contents($template_path);
            $this->response = array('msg' => 'Установлен дефолтный шаблон', 'template' => $template);
            return;
        }
        
        $post_template = waRequest::post('reviewsplus-template');
        
        if(!isset($post_template) && empty($post_template)) {
            
            $this->errors = 'Шаблон не передан';            
            return;
        }
        
        $template_path = shopReviewsplusPlugin::getPluginPath('templates', 'frontend/reviews-list.html');
        
        $template = file_get_contents($template_path['path']);
        
        if($template != $post_template) {
            $template_path = wa()->getDataPath($path, false, 'shop', true);
            waFiles::write($template_path, $post_template);
            $this->response = array('msg' => 'Шаблон сохранен', 'chg' => 1);
            return;
        } else {
            $this->errors = 'Шаблон не изменен';
            return;
        }        
    }
    
    public function arSettingsSaveAction() {
        
        $settings = waRequest::post('arsettings');
        
        if(isset($settings['state'])){
            $this->app_settings_model->set($this->plugin_id, 'arstate', 1);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'arstate', 0);
        } 
        
        if(isset($settings['reviews_per_page'])){
            $this->app_settings_model->set($this->plugin_id, 'ar_reviews_per_page', (int)$settings['reviews_per_page']);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'ar_reviews_per_page', 0);
        }
        
        if(isset($settings['reviews_answers'])){
            $this->app_settings_model->set($this->plugin_id, 'ar_reviews_answers', 1);
        } else {
            $this->app_settings_model->set($this->plugin_id, 'ar_reviews_answers', 0);
        }
        
        $this->response = array('msg' => 'Сохранено');
        
    }
    
    
    /**
     * Удаление доп. поля из таблиц
     * @return type
     */
    public function delReviewsFieldAction() {
        
        $field_id = waRequest::post('field_id', '');        
        
        if(!isset($field_id) || empty($field_id)) {
            
            $this->errors = 'Не задано поле';
            return;
        }
        
        
        $name_id = $this->fields_model->select('name_id')
                                      ->where('id = i:id', array('id' => $field_id))
                                      ->fetchField();
        
        //Удаляем из таблицы полей (reviewsplus_fields)
        $res = $this->fields_model->deleteById($field_id);
        
        if(!$res) {
            shopReviewsplusPlugin::addLog('Не удалось удалить поле '.$name_id.' Ошибка БД', 'reviewsplus-fields-error.log');
            $this->errors = 'Не удалось удалить поле';
            return;
        }
        
        //Удаляем из таблицы доп. данных (reviewsplus_dop)
        if($name_id) {
            $tt = $this->wa_model->query('ALTER TABLE  `shop_reviewsplus_dop` DROP  `'.$this->wa_model->escape($name_id).'`');
            
            if(!$tt) {
                shopReviewsplusPlugin::addLog('Не удалось удалить доп. данные для поля '.$name_id.' Ошибка БД', 'reviewsplus-fields-error.log');                
            }
        }
        
        $this->response = array('msg' => 'Удалено');            
    }
    
    /**
     * Редактирование доп. поля
     * @return type
     */
    public function editReviewsFieldAction() {
        
        $field_id = waRequest::post('field_id', '');
        
        if(!isset($field_id) || empty($field_id)) {
            
            $this->errors = 'Не задано поле'; 
            return;
        }
        
        $field = $this->fields_model->getById($field_id);
        
        if(!isset($field) || empty($field)) {
            
            $this->errors = 'Нет полей';  
            return;
        }
        
        $this->response['field'] = $field;    
        
    }
    
    /**
     * Запись нового или отредактированого поля
     * @return type
     */
    public function saveReviewsFieldAction() {
        
        $fields = waRequest::post('fields', array());
        
        if(empty($fields)) {            
            $this->errors = 'Нет настроек';
            return;
        }
        
        //Если есть id, значит это редактируемое поле
        $id = waRequest::post('id', 0);        
        
        if($id != 0) {
            $res = $this->fields_model->updateById($id, $fields);
            
            if(!$res) {
                shopReviewsplusPlugin::addLog('Не удалось обновить поле '.$fields['name'].' Ошибка БД', 'reviewsplus-fields-error.log');
                $this->errors = 'Не удалось обновить поле '.$fields['name'];
                return;
            }
            
            $this->response['msg'] = 'Сохранено';
            return;
        }
        
        //Добавление нового поля
        
        $fields['name'] = strip_tags($fields['name']);
        
        $fields['name_id'] = substr(strtolower(shopHelper::transliterate($fields['name'])), 0, 50);
        
        if($this->fields_model->getByField('name_id', $fields['name_id'])) {
            $this->errors = 'Такое поле уже существует';
            return;
        }
        
        if(!is_numeric($fields['sort'])) {
            $this->errors = 'Сортировка должна быть числом!';
            return;
        }
        
        $res = $this->fields_model->insert($fields);
        
        if(!$res) {
            shopReviewsplusPlugin::addLog('Не удалось добавить поле '.$fields['name'].' Ошибка БД', 'reviewsplus-fields-error.log');
            $this->errors = 'Не удалось записать в БД';
            return;
        }
        
        $sql = "ALTER TABLE  `shop_reviewsplus_dop` ADD `".$this->wa_model->escape($fields['name_id'])."` ";
        
        if($fields['type'] === 'textarea') {
            $sql .= 'MEDIUMTEXT ';
        } else if($fields['type'] === 'text') {
            $sql .= 'VARCHAR(100) ';
        } else if($fields['type'] === 'rate') {
            $sql .= 'TINYINT(2) ';
        }
        
        $sql .= 'NULL';
        
        $tt = $this->wa_model->query($sql); 
        
        if(!$tt) {
            shopReviewsplusPlugin::addLog('Не удалось добавить поле '.$fields['name'].' в shop_reviewsplus_dop. Ошибка БД', 'reviewsplus-fields-error.log');
            $this->errors = 'Не удалось добавить доп. поле';
            return;
        }
        
        $this->response = array('msg' => 'Сохранено');
        
    }
    
    /**
     * Построение таблицы доп. полей в настройках
     * @return type
     */
    public function buildFieldsTableAction() {
        
        $fields = $this->fields_model->order('type DESC, sort ASC')->fetchAll();
        
        if(!isset($fields) || empty($fields)) {
            $this->errors = 'Записи отсутствуют';
            return;
        }
        
        $html = '';
        
        foreach($fields as $field) {
            
            $html .= '<tr data-reviewsplus-fields-id="'.$field['id'].'">';
            
            $html .= "<td style='text-align: center;'>";
            $html .= "<a href='javascript:void(0);' class='reviewsplus-field-edit' title='Редактировать' >";
            $html .= "<i class='icon16 edit'></i></a>";
            $html .= "</td>";
            
            
            $html .= "<td>";
            if(isset($field['name_id']) && !empty($field['name_id'])) {
                $html .= $field['name_id'];
            } else {
                $html .= "-";
            }
            $html .= "</td>";
            
            $html .= "<td>";
            if(isset($field['type']) && !empty($field['type'])) {
                $html .= $field['type'];
            } else {
                $html .= "-";
            }
            $html .= "</td>";
            
            $html .= "<td class='reviewsplus-fields-name'>";
            if(isset($field['name']) && !empty($field['name'])) {
                $html .= $field['name'];
            } else {
                $html .= "-";
            }
            $html .= "</td>";
            
            $html .= "<td style='text-align: center;'>";
            if(isset($field['required']) && $field['required'] == 1) {
                $html .= "<i class='icon16 yes'></i>";
            } else {
                $html .= "<i class='icon16 no'></i>";
            }
            $html .= "</td>";
            
            $html .= "<td style='text-align: center;'>";
            if(isset($field['show']) && $field['show'] == 1) {
                $html .= "<i class='icon16 yes'></i>";
            } else {
                $html .= "<i class='icon16 no'></i>";
            }
            $html .= "</td>";
            
            $html .= "<td style='text-align: center;'>";
            if(isset($field['sort'])) {
                $html .= htmlspecialchars($field['sort']);
            } else {
                $html .= 0;
            }            
            $html .= "</td>";
            
            $html .= "<td style='text-align: center;'>";
            if(isset($field['delete']) && $field['delete']) {
                $html .= "<a href='javascript:void(0);' class='reviewsplus-field-delete' title='Удалить' >";
                $html .= "<i class='icon16 delete'></i></a>";
            } else {
                $html .= "<i class='icon16 no'></i>";
            }
            $html .= "</td>";
        }
        
        $html .= "</tr>";
        
        $this->response['html'] = $html;        
    }
    
    /**
     * Удаление отзыва (не полное)
     * @return type
     */
    public function delReviewAction() {
        
        $id = waRequest::post('id', 0, 'int');
        
        if($id == 0) {
            $this->errors = 'Не передан id';
            return;
        }
        
        $type = waRequest::post('type', '');
        
        if(empty($type)) {
            $this->errors = 'Не передан тип';
            return;
        }
        
        if($type === 'moderated') {
            //Если тип "На модерации"
            $del_review = $this->moderate_model->deleteById($id);
            $del_dop = $this->dop_model->deleteByField('moderate_id', $id);
        } elseif ($type === 'published') {
            //Если опубликованый
            $del_review = $this->reviews_model->changeStatus($id, 'deleted');            
        } elseif ($type === 'deleted') {
            //Если удаленный - удаляем полностью и подчищаем доп. данные
            $del_review = $this->reviews_model->deleteById($id);
            $this->reviews_model->repair();
            $del_dop = $this->dop_model->deleteByField('review_id', $id);
        }
        
        if(!$del_review) {
            
            shopReviewsplusPlugin::addLog('Не удалось удалить отзыв. id отзыва - '.$id.' тип: '.$type, 'reviewsplus-error.log');
            $this->errors = 'Не удалось удалить отзыв';
            return;
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' удалил отзыв с id: '.$id.' тип: '.$type, 'reviewsplus-report.log');
        }
        
        
        //Считаем кол-во        
        if($type === 'moderated') {
            $count = $this->moderate_model->countAll();
        } else {            
            $count = $this->reviews_model->countByField(array('status' => 'approved', 'depth' => 0));
        }
        
        $dcount = $this->reviews_model->countByField('status', 'deleted');        
        
        $this->response = array('msg' => 'Отзыв удален', 'count' => $count, 'dcount' => $dcount);       
        
    }
    
    /**
     * Одобрямс отзыв
     * @return type
     */
    public function publishModeratedReviewAction() {
        
        $id = waRequest::post('id', 0, 'int');
        
        if($id == 0) {
            $this->errors = 'Не передан id';
            return;
        }
        
        $review = $this->moderate_model->getById($id);
        
        unset($review['id']);
        
        $review_id = $this->reviews_model->add($review);
        
        if(!$review_id) {
            
            shopReviewsplusPlugin::addLog('Не удалось перенести отзыв '.$id.' из moderated в основную таблицу', 'reviewsplus-error.log');
            $this->errors = 'Не удалось добавить отзыв.';
            return;
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' одобрил отзыв: '.$review_id, 'reviewsplus-report.log');            
        }
        
        $msg = 'Отзыв опубликован.';
        
        $email_en = (isset($review['email']) && !empty($review['email'])) ? TRUE : FALSE;
        if($email_en) {
            $review['coup_rev_id'] = $review_id;
            shopReviewsplusPlugin::sendCoupon($review);
        }    
        
        
        //Переносим доп. поля с модерации
        $dop = $this->dop_model->updateByField('moderate_id', $id, array('moderate_id' => 0, 'review_id' => $review_id));
        
        if(!$dop) {
            shopReviewsplusPlugin::addLog('Не удалось переписать доп. поля для отзыва на модерации. id '.$review_id, 'reviewsplus-error.log');
            $msg .= 'Не удалось переписать доп. поля для отзыва на модерации.';
        }
        
        $mr_delete = $this->moderate_model->deleteById($id);
        
        if(!$mr_delete) {
            shopReviewsplusPlugin::addLog('Не удалось удалить отзыв на модерации из БД. id '.$review_id, 'reviewsplus-error.log');
            $msg .= 'Не удалось удалить отзыв на модерации из БД.';
        } else {
            $count = $this->moderate_model->countAll();            
        }
        
        $pcount = $this->reviews_model->countByField(array('status' => 'approved', 'depth' => 0)); 
        
        $this->response = array('msg' => $msg, 'count' => $count, 'pcount' => $pcount);
        
    }
    
    /**
     * Получаем отзывы различных типов
     * @return type
     */
    public function getReviewsAction() {  
        
        $type = waRequest::post('type', '');
        
        if(empty($type)) {
            $this->errors = 'Не передан тип';
            return;
        }
        
        $page = waRequest::post('page', 1, 'int');
        
        $options = array();
        
        $limit = waRequest::cookie('reviewsplus-pp', 5, 'int');        
        
        if($limit !== 0) {
           $options = array('limit' => $limit);
        }        
        
        if($type === 'moderated') {
            
            $count = $this->moderate_model->countAll();
            
            if($limit != 0 && ($count > $limit)) {
                $pages_count = ceil((float)$count / $limit);                
                $offset = ($page - 1) * $limit;
                $limit = $offset.','.$limit;
            }
            
            if($limit === 0) {
                $reviews = $this->moderate_model->order('datetime DESC')->fetchAll('id');
            } else {          
                $reviews = $this->moderate_model->order('datetime DESC')
                                                ->limit($limit)
                                                ->fetchAll('id');
            }
            
        } else { 
            
            if($type === 'published') {
                $count = $this->reviews_model->countByField(array('status' => 'approved', 'depth' => 0));
                if($limit != 0 && ($count > $limit)) {
                    $pages_count = ceil((float)$count / $limit);                
                    $offset = ($page - 1) * $limit;
                    $options['offset'] = $offset;                    
                }
                
                $options['where'] = array('status' => 'approved', 'depth' => 0);
            } elseif ($type === 'deleted') {
                
                $count = $this->reviews_model->countByField('status', 'deleted');
                if($limit != 0 && ($count > $limit)) {
                    $pages_count = ceil((float)$count / $limit);                 
                    $offset = ($page - 1) * $limit;
                    $options['offset'] = $offset;                    
                } 
                
                $options['where'] = array('status' => 'deleted');
            }     
            
            $reviews = $this->reviews_model->getList('*', $options);            
        }
        
        if(!isset($reviews) || empty($reviews)) {
            $this->errors = 'Не удалось получить отзывы '.$type;
            return;
        }
                
        foreach ($reviews as $review) {
            $product_ids[] = $review['product_id'];
        }
        
        $reviews_ids = array_keys($reviews);
        
        $product_model = new shopProductModel();
        $product_ids = array_unique($product_ids);
        $products = $product_model->getByField('id', $product_ids, 'id');        
        foreach ($reviews as $val => $review) {            
            if (isset($products[$review['product_id']])) {
                $product = $products[$review['product_id']];
                $reviews[$val]['p_name'] = $product['name'];
            }
        }        
        
        //Читаем доп. поля        
        if($type == 'moderated') {
            $sql_field = 'moderate_id';            
        } else {
            $sql_field = 'review_id';
        }
        
        $dop_fields = $this->dop_model->getByField($sql_field, $reviews_ids, $sql_field);
        
        //Ответы
        $comments = $this->reviews_model->getByField('parent_id', $reviews_ids, 'id');
        
        foreach($comments as $comment) {
            unset($reviews[$comment['id']]);            
            $reviews[$comment['parent_id']]['comments'][] = $comment;
        }
        
        //Получаем таблицу существующих полей        
        $fields = $this->fields_model->select('name, name_id, type')->fetchAll('name_id');
        
        if(!$fields) {
            $fields = array();
        }
        
        $view = wa()->getView();  
        
        if(isset($pages_count) && $pages_count !== 0) {
            $view->assign('pages_count', $pages_count);
        }
                
        $view->assign('cpage', $page);        
        $view->assign('fields', $fields);
        $view->assign('reviews', $reviews);
        $view->assign('dop_fields', $dop_fields);
        $view->assign('type', $type);        
        
        $path = shopReviewsplusPlugin::getPluginPath('templates');
        $html = $view->fetch($path['path'].'backend/form.html');
        
        $this->response['html'] = $html;        
    } 
    
    /**
     * Получем отзыв для редактирования
     * @return type
     */
    public function getEditedReviewAction() {
        
        $id = waRequest::post('id', 0, 'int');
        
        if($id === 0) {
            $this->errors = 'Не передан id';
            return;
        }
        
        $type = waRequest::post('type', '');
        
        if(empty($type)) {
            $this->errors = 'Не указан тип';
            return;
        }
        
        //Получаем основные поля
        switch ($type) {
            
            case 'moderated':
                $review = $this->moderate_model->getById($id);
                break;
            
            case 'published':
            case 'deleted':    
                $review = $this->reviews_model->getById($id);
                break;            
        }        
        
        if(!$review) {
            $this->errors = 'Не удалось получить отзыв';
            return;
        }
        
        
        //Получаем доп. поля
        if($type === 'moderated') {
            $dop = $this->dop_model->getByField('moderate_id', $id);
        } else {
            $dop = $this->dop_model->getByField('review_id', $id);
        }
        
        if(!$dop) {
            $dop = array();
        }
        
        $this->response = array('review' => $review, 'dop' => $dop);
        
    }
    
    //Записываем отредактированный отзыв
    public function saveEditReviewAction() {
        
        $id = waRequest::post('id', 0, 'int');
        
        if($id === 0) {
            $this->errors = 'Не передан id';
            return;
        }
        
        $type = waRequest::post('type', '');
        
        if(empty($type)) {
            $this->errors = 'Не передан type';
            return;
        }
        
        //Основные и доп. поля
        $main = waRequest::post('main', array());         
        $dop = waRequest::post('dop', array());
        
        if($type === 'moderated') {
            //Если модерируемый отзыв
            $tt = $this->moderate_model->updateById($id, $main);
            $zz = $this->dop_model->updateByField('moderate_id', $id, $dop);
        } else {
            //Если нет
            $tt = $this->reviews_model->updateById($id, $main);
            
            if($this->dop_model->getByField('review_id', $id)) {
                //Если доп. поля уже есть
                $zz = $this->dop_model->updateByField('review_id', $id, $dop);
            } else {
                //Если нет
                $dop['review_id'] = $id;
                $zz = $this->dop_model->insert($dop);
            }            
        }
        
        if(!$tt) {
            shopReviewsplusPlugin::addLog('Не удалось отредактировать отзыв id: '.$id.' Тип: '.$type, 'reviewsplus-error.log');
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' отредактировать отзыв с id: '.$id.' Тип: '.$type, 'reviewsplus-report.log');
        }
        
        
        $this->recalcRating($id);
        
        $this->response = array('id' => $id, 'type' => $type, 'main' => $main, 'dop' => $dop, 'zz' => $zz, 'tt' => $tt);
        
    }
    
    private function recalcRating($id) {
        
        $pid = $this->reviews_model->select('product_id')
                                   ->where('id = i:id', array('id' => $id))
                                   ->fetchField();
        
        if(!$id) {
            return;
        }
        
        $temp = $this->reviews_model->getByField(array('product_id' => $pid, 'parent_id' => 0, 'status' => 'approved'), true);
        
        if(!$temp) {
            return;
        }
        
        $rating = 0.0;
        
        $rrr = array();
        
        foreach($temp as $key) {
            
            if($key['rate']) {                
               $rating += (float)$key['rate']; 
            }            
        }
        
        $new_rating = (float)$rating/count($temp);
        
        $product_model = new shopProductModel();
        
        $product_model->updateById($pid, array('rating' => $new_rating, 'rating_count' => count($temp)));
        
        return; 
    }
    
    /**
     * Восстановление из удаленных
     * @return type
     */
    public function returnDeletedAction() {
        
        $id = waRequest::post('id', 0, 'int');
        
        if($id === 0) {
            $this->errors = 'Не передан id';
            return;
        }
        
        $tt = $this->reviews_model->changeStatus($id, 'approved');
        
        if(!$tt) {
            shopReviewsplusPlugin::addLog('Не удалось восстановить отзыв id: '.$id, 'reviewsplus-error.log');
            $this->errors = 'Не удалось восстановить отзыв';
            return;
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' восстановил отзыв с id: '.$id, 'reviewsplus-report.log');
        }
        
        $pcount = $this->reviews_model->countByField(array('status' => 'approved', 'depth' => 0));
        $dcount = $this->reviews_model->countByField('status', 'deleted');
        
        $this->response = array('msg' => 'Отзыв восстановлен', 'pcount' => $pcount, 'dcount' => $dcount);        
    }
    
    /**
     * Удаление ответа
     * @return type
     */
    public function delCommentAction() {
        
        $comm_id = waRequest::post('comm_id', 0, 'int');
                
        if($comm_id === 0) {
            $this->errors = 'Не указан id комментария';
            return;
        }
        
        $tt = $this->reviews_model->changeStatus($comm_id, 'deleted');
        
        if(!$tt) {
            shopReviewsplusPlugin::addLog('Не удалось удалить ответ id: '.$comm_id, 'reviewsplus-error.log');
            $this->errors = 'Не удалось удалить ответ';
            return;
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' удалил ответ с id: '.$id, 'reviewsplus-report.log');
        }        
        
        $pcount = $this->reviews_model->countByField('status', 'approved');
        $dcount = $this->reviews_model->countByField('status', 'deleted');
        
        $this->response = array('msg' => 'Комментарий удален', 'pcount' => $pcount, 'dcount' => $dcount);
        
    }
    
    /**
     * Получение ответа для редактирования
     * @return type
     */
    public function getCommentAction() {
        
        $comm_id = waRequest::post('comm_id', 0, 'int');
        
        if($comm_id === 0) {
            $this->errors = 'Не указан id ответа';
            return;
        }
        
        $comm = $this->reviews_model->select('title, id, text')
                                    ->where('id = i:id', array('id' => $comm_id))
                                    ->fetchAll();
        
        if(!$comm) {
            $this->errors = 'Не удалось прочитать комментарий';
            return;
        }
        
        $this->response = $comm;        
    }
    
    /**
     * Запись отредактированного или нового ответа
     * @return type
     */    
    public function saveCommentAction() {
        
        $main = waRequest::post('reviewsplus-comment', array());
        
        if(empty($main)) {
            $this->errors = 'Пустые данные';
            return;
        }
        
        if(!empty($main['parent_id'])) {
            //Если добавляем ответ
            unset($main['id']);
            $parent_id = $main['parent_id'];
            unset($main['parent_id']);
            
            $a_review = $this->reviews_model->getByField('id', $parent_id);
            
            if(!$a_review) {
                $this->errors = 'Не удалось получить родительский комментарий';
                return;
            }
            
            $main['product_id'] = $a_review['product_id'];
            $main['status'] = 'approved';
            $main['contact_id'] = wa()->getUser()->getId();
            $main['auth_provider'] = 'user';      
            $main['title'] = $main['title'];
            $main['text'] = $main['text'];
            
            $tt = $this->reviews_model->add($main, $parent_id);
            
            if(!$tt) {
                $this->errors = 'Не удалось обновить ответ'.$tt;
                return;
            }
            
            $msg = 'Ответ добавлен';
            
        } else {
            //Если редактируем
            if(!isset($main['id']) || empty($main['id'])) {
                $this->errors = 'Не указан id';
                return;
            }
            
            $id = $main['id'];
            unset($main['id']);
            $tt = $this->reviews_model->updateById($id, $main);
            
            if(!$tt) {
                $this->errors = 'Не удалось обновить ответ';
                return;
            }
            
            $msg = 'Ответ отредактирован';
        }
        
        $this->response = $msg;        
    }
    
    /**
     * Полное удаление всех удаленных
     * @return type
     */
    public function delAllReviewsAction() {
        
        $deleted_reviews = $this->reviews_model->select('*')
                                               ->where('status = "deleted"')
                                               ->fetchAll('id');
        
        if(!$deleted_reviews) {
            $this->errors = 'Нет удаленных отзывов';
            return;
        }
        
        $deleted_ids = array_keys($deleted_reviews);
        
        $tt = $this->reviews_model->deleteByField('status', 'deleted');
        
        if(!$tt) {
            shopReviewsplusPlugin::addLog('Не удалось полностью удалить все  отзывы', 'reviewsplus-error.log');            
        } else {
            shopReviewsplusPlugin::addLog('Пользователь '.wa()->getUser()->getId().' удалил все отзывы', 'reviewsplus-report.log');
        }  
        
        $this->reviews_model->repair();        
        
        $tt_dop = $this->dop_model->deleteByField('review_id', $deleted_ids);
        
        if(!$tt_dop) {
            shopReviewsplusPlugin::addLog('Не удалось удалить доп. поля для всех удаляемых отзывов', 'reviewsplus-error.log');
        }
                
        $this->response = array('tt' => $tt, 'tt_dop' => $tt_dop);        
    }
    
    public function verifyAkisKeyAction() {
        
        if(!class_exists('Akismet')) {       
            $this->errors = 'Не удалось подлючить класс akismet';
            return;                    
        }
        
        $key = waRequest::post('key', '');
        
        if(empty($key)) {
            $this->errors = 'Не передан ключ';
            return;
        }
        
        $url = urlencode(wa()->getRootUrl(true));
        
        $akismet = new Akismet($url, $key);
        
        if($akismet->isKeyValid()) {
            $this->response['msg'] = 'Правильный ключ';            
        } else {
            $this->errors = 'Неправильный ключ';
            return;
        }                
    }
    
    
    
}
