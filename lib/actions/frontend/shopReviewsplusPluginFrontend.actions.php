<?php
/** 
 * @desc
 * Обработка экшенов фронтенда
 * для плагина ReviewsPlus
 * 
 * @author мастерская BNP <support@byloneprosto.ru> 
 */

class shopReviewsplusPluginFrontendActions extends waJsonActions
{   

    public function __construct() {
        $this->reviews_model = new shopProductReviewsModel(); 
        $this->fields_model = new shopReviewsplusPluginFieldsModel();
        $this->dop_model = new shopReviewsplusPluginDopModel();
        $this->moderate_model = new shopReviewsplusPluginModeratedModel();
        $this->plugin_settings = shopReviewsplusPlugin::getPluginSettings();
    }      
    
    /**
     * Добавление отзыва
     * @return type
     */
    public function addReviewAction() {
        
        $captcha = waRequest::post('captcha');
        
        if(isset($captcha) && empty($captcha)) {
            $this->errors[] = 'Введите капчу';
            $this->errors['captcha_refresh'] = TRUE;            
        } elseif (!empty ($captcha)) {
            if(!wa()->getCaptcha()->isValid()) {
                $this->errors[] = 'Неправильно введена капча';
                $this->errors['captcha_refresh'] = TRUE;                
            }            
        }
        
        if(!empty($this->errors)) {
            return;
        }        
                
        $product_id = waRequest::post('product_id', 0, 'int');
        
        if($product_id == 0) {
            $this->errors[] = 'Не задан id продукта';
            return;
        }
        
        //Получаем основные поля
        $main = waRequest::post('main', array());
        
        if(empty($main)) {
            $this->errors[] = 'Не переданы основные данные';
            return;
        }      
        
        $ip = waRequest::getIp();
        
        if($ip) {
            
            $ip = ip2long($ip);
            if ($ip > 2147483647) {
                $ip -= 4294967296;
            } 
            
            $main['ip'] = $ip;
        } else {
            $main['ip'] = NULL;
        }
        
        $akis_state = $this->plugin_settings['akis_state'];
        $spam = FALSE;
        $msg = '';
        
        if(isset($akis_state) && $akis_state == 1) {
            $akis_check = $this->checkAkis($main);
            
            if(!$akis_check['error'] && $akis_check['spam']) {
                $spam = TRUE;
                $msg .= $akis_check['msg'];
            } elseif(!$akis_check['spam']) {
                $msg .= $akis_check['msg'];
            } elseif($akis_check['error']) {
                shopReviewsplusPlugin::addLog('Не удалось проверить на спам. Ошибка: '.$akis_check['error_msg'], 'reviewsplus-error.log'); 
            }       
        }
        
        //Задаем id продукта
        $main['product_id'] = $product_id;        
        $main['rate'] = (float)$main['rate'];        
        
        $valid = $this->validateReview($main);
        
        if($valid['error']) {
            foreach($valid['error_msg'] as $error) {
                $this->errors[] = $error;                
            }                        
            return;
        }
        
        $main = $valid['main'];
        
        //Включена ли модерация?
        $moderate = $this->plugin_settings['moderate'];
        
        $mod = (isset($moderate) && $moderate == 1) ? TRUE : FALSE;  
        
        if($spam) {
            $mod = TRUE;
            $main['spam'] = 1;
        }
        
        //Отправка купона       
        $send_coupon = FALSE;
        
        if($mod) {    
            $main['datetime'] = date('Y-m-d H:i:s');
            $review_id = $this->moderate_model->insert($main);
            $msg .= 'Отзыв отправлен на модерацию';
        } else {
            $review_id = $this->reviews_model->add($main);
            $msg .= 'Отзыв опубликован';  
            $send_coupon = TRUE;
        }  
        
        shopReviewsplusPlugin::addLog('Продукт: '.$product_id.' id отзыва: '.$review_id.'. '.$msg, 'reviewsplus-report.log');
                        
        if(!$review_id) {
            $msg .= 'Не удалось добавить отзыв';
            shopReviewsplusPlugin::addLog($msg.' Ошибка: не удалось записать в БД. Продукт: '.$product_id, 'reviewsplus-error.log');
            $this->errors = $msg;
            return;
        } 
        
        if($send_coupon) {
            $main['coup_rev_id'] = $review_id;
            $tt = shopReviewsplusPlugin::sendCoupon($main);
            
            if(!$tt['error']) {
                $msg .= $tt['msg'];
            }
        }
        
        $notif = $this->plugin_settings['email'];
        
        if(isset($notif) && !empty($notif)) {            
            $this->sendNotif($notif, $product_id, $msg, $main);
        }
        
        //Получаем доп. поля
        $dop = waRequest::post('dop', array());
        
        if(empty($dop)) {
            $this->response = array('msg' => $msg);
            return;
        }
        
        if($mod) {
            $dop['moderate_id'] = $review_id;            
        } else {
            $dop['review_id'] = $review_id;
        }
        
        $tt = $this->dop_model->insert($dop);
        
        if(!$tt) {
            shopReviewsplusPlugin::addLog('Не удалось добавить доп. данные для комментария '.$review_id, 'reviewsplus-error.log');
        }
        
        $this->response = array('msg' => $msg);        
             
    }
    
    
    private function checkAkis($comment = array()) {
        
        if(!class_exists('Akismet')) {       
            return array('error' => TRUE, 'error_msg' => 'Не удалось подлючить класс akismet');                             
        }
        
        if(empty($comment)) {
            return array('error' => TRUE, 'error_msg' => 'Не переданы данные');
        }
        
        if(!isset($comment['ip']) && empty($comment['ip'])) {
            return array('error' => TRUE, 'error_msg' => 'Не удалось определить ip');
        } else {
            $comment['ip'] = long2ip($comment['ip']);
        }
        
        $api_key = $this->plugin_settings['akis_key'];                
        
        if(!isset($api_key) || empty($api_key)) {
            return array('error' => TRUE, 'error_msg' => 'Не удалось получить api-ключ');
        }
        
        $url = wa()->getRootUrl(true);        
        $akismet = new Akismet($url, $api_key);
        
        if(isset($comment['name']) && !empty($comment['name'])) {
            $akismet->setCommentAuthor($comment['name']);
        }
        
        if(isset($comment['email']) && !empty($comment['email'])) {
            $akismet->setCommentAuthorEmail($comment['email']);
        }
        
        if(isset($comment['site']) && !empty($comment['site'])) {
            $akismet->setCommentAuthorURL($comment['site']);
        }
        
        if(isset($comment['text']) && !empty($comment['text'])) {
            $akismet->setCommentContent($comment['text']);
        }
        
        if ($akismet->isCommentSpam()) {
            return array('error' => FALSE, 'spam' => TRUE, 'msg' => 'Подозрение на спам. ');
        } else {
            return array('error' => FALSE, 'spam' => FALSE, 'msg' => 'Проверено на спам. ');
        }
        
    }
    
    private function sendNotif($email, $pid = 0, $action = '', $rev_data = array()) {
        
        if($pid === 0) {
            return;
        }
        
        $model = new shopProductModel();
        
        $p_name = $model->select('name')
                        ->where('id = i:id', array('id' => $pid))
                        ->fetchField();
        
        $body = 'Уважаемый администратор!<br/><br/> Для товара <b>'.htmlspecialchars($p_name).'</b> добавлен отзыв<br/>';
        
        if(isset($rev_data['title']) && !empty($rev_data['title'])) {
            $body .= '<br/><b>Заголовок отзыва:</b> '. htmlspecialchars($rev_data['title']);
        }
        
        if(isset($rev_data['text']) && !empty($rev_data['text'])) {
            $body .= '<br/><b>Текст отзыва:</b> '. htmlspecialchars($rev_data['text']);
        }
        
        
        $body .= '<br/><br/>'.$action;
        
        $subject = 'Добавлен новый отзыв о товаре';
        
        $mail_message = new waMailMessage($subject, $body);
        
        $from = wa()->getSetting('email', '', 'shop');
        
        $mail_message->setFrom($from, 'Плагин Reviews Plus');
        
        $mail_message->setTo($email);
        
        $tt = $mail_message->send();
		
	if(!$tt) {			
            shopReviewsplusPlugin::addLog('Не удалось отправить уведомление о новом отзыве', 'reviewsplus-error.log');
	}
        
    }
    
    private function validateReview($main = array()) {
        
        if(empty($main)) {
            return array('error' => TRUE, 'error_msg' => 'Отзыв не передан');
        }
        
        $config = wa('shop')->getConfig();
        $errors = array();
        $captcha_refresh = FALSE;
        
        $contact_id = (int)$this->getUser()->getId();
        
        if ($contact_id) {
            
            $main['contact_id'] = $contact_id;
            $main['auth_provider'] = 'user';
        } else {
            
            if ($config->getGeneralSettings('require_authorization', false)) {
                $errors[] = _w('Only authorized users can post reviews');                
            }
            
            $main['contact_id'] = 0;  
            $main['auth_provider'] = 'guest';
        }
        
        if(!isset($main['name']) || empty($main['name'])) {
            $main['name'] = 'Аноним';
        } else {
            if (mb_strlen($main['name']) > 255) {
                $errors[] = _w('Name length should not exceed 255 symbols');                
            }
        }
        
        if(!isset($main['site']) || empty($main['site'])) {
            $main['site'] = NULL;
        } else {
           
           if(strpos($main['site'], '://') === false) {
               $main['site'] = "http://" . $main['site'];  
           }
           
           $validator = new waUrlValidator();
           if(!$validator->isValid($main['site'])) {
               $errors[] = _w('Site URL is not valid');               
           }            
        }
        
        if(!isset($main['title']) || empty($main['title'])) {
            $main['title'] = 'Без заголовка';
        }
        
        if(!isset($main['email']) || empty($main['email'])) {
            $main['email'] = NULL;
        } else {
            
            $validator = new waEmailValidator();
            if (!$validator->isValid($main['email'])) {
                $errors[] = _w('Email is not valid');                
            }
        }   
        
        if(isset($main['text']) && !empty($main['text'])) {
            if (mb_strlen($main['text']) > 4096) {
                $errors[] = _w('Review length should not exceed 4096 symbols');                
            }
        }
        
        if(!empty($errors)) {
            return array('error' => TRUE, 'error_msg' => $errors);
        } else {
            return array('error' => FALSE, 'main' => $main);
        }
        
    }
    
    /**
     * Удаление отзыва
     * @return type
     */
    public function delReviewAction() {
        
        if(!wa()->getUser()->getRights('shop', 'products')) {
            $this->errors = 'У вас нет прав для удаления отзывов';
            return;
        }
        
        $review_id = waRequest::post('review_id', 0, 'int');
        
        if($review_id == 0) {
            $this->errors = 'Нет id комментария для удаления';
            return;
        }
        
        $tt = $this->reviews_model->updateByField('id', $review_id, array('status' => 'deleted'));
        
        if(!$tt) {
            
            shopReviewsplusPlugin::addLog('Не удалось удалить отзыв с id: '.$review_id, 'reviewsplus-error.log');            
            $this->errors = 'Что-то пошло не так с удалением';
            return;
        } else {
            $this->response = 'Все в порядке';
            return;
        }
        
    }
    
    /**
     * Постраничный вывод
     * @return type
     */
    public function showReviewsPageAction() {
        
        $pid = waRequest::post('pid', 0, 'int');
        
        if($pid === 0) {
            $this->errors = 'Не указан pid';
            return;
        }
        
        $page = waRequest::post('page', 1, 'int');
        
        $html = shopReviewsplusPlugin::buildReviewsList($pid, $page);
        
        $this->response = $html;
        
    }
}