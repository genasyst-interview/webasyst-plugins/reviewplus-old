<?php
/**
 * shopReviewsmasterPluginFrontendActions 
 * 
 * @author мастерская BNP <support@byloneprosto.ru> 
 */

class shopReviewsplusPluginFrontendGetAllReviewsAction extends waViewAction
{   
    
    protected $fields_model, $dop_model, $reviews_model;
    
    public function execute() {
        
        $this->fields_model = new shopReviewsplusPluginFieldsModel();
        $this->dop_model = new shopReviewsplusPluginDopModel();        
        $this->reviews_model = new shopProductReviewsModel();       
        
        if (!file_exists($this->getTheme()->path.'/all-reviews.html')) {
            $this->redirect(wa()->getRouteUrl('shop/frontend'));
        }
        
        $sett = shopReviewsplusPlugin::getPluginSettings();        
        
        if(!isset($sett['arstate']) || $sett['arstate'] != 1) {
            $this->redirect(wa()->getRouteUrl('shop/frontend'));
        }
        
        $options = array();
        
        if(isset($sett['ar_reviews_per_page']) && !empty($sett['ar_reviews_per_page'])) {
            $limit = $sett['ar_reviews_per_page'];
            $options = array('limit' => $limit);
        } else {
            $limit = 0;
        }
        
        $page = waRequest::get('page', 1, waRequest::TYPE_INT);
        
        $options['where'] = array('status' => 'approved', 'depth' => 0);
        
        $count = $this->reviews_model->countByField(array('status' => 'approved', 'depth' => 0));     
        $this->view->assign('count', $count);
        
        if($limit != 0 && ($count > $limit)) {
            $pages_count = ceil((float)$count / $limit);                
            $offset = ($page - 1) * $limit;
            $options['offset'] = $offset; 
            $this->view->assign('pages_count', $pages_count);
        }          
        
        $reviews = $this->reviews_model->getList('*,is_new,contact,product', $options);
        
        if(isset($reviews) && !empty($reviews)) {
            
            $reviews_ids = array_keys($reviews);   
        
            $fields = $this->fields_model->select('*')->fetchAll('name_id');
        
            if(!$fields) {
                $fields = array();
            }
        
            $dop_reviews = $this->dop_model->getByField('review_id', $reviews_ids, 'review_id');
            
            foreach($dop_reviews as $rid => $dop) {            
                
                foreach($dop as $val => $key) {                
                
                    if(array_key_exists($val, $fields)) {
                    
                        if($fields[$val]['type'] === 'rate') {
                        
                            $reviews[$rid]['dop_rate'][] = array('name' => $fields[$val]['name'],
                                                                 'name_id' => $fields[$val]['name_id'],
                                                                 'value' => $key
                                                                );
                        
                        } else if($fields[$val]['type'] === 'text') {
                            
                            $reviews[$rid]['dop_text'][] = array('name' => $fields[$val]['name'],
                                                                 'name_id' => $fields[$val]['name_id'],
                                                                 'value' => $key
                                                                );
                        
                        } else if($fields[$val]['type'] === 'textarea') {
                        
                            $reviews[$rid]['dop_textarea'][] = array('name' => $fields[$val]['name'],
                                                                 'name_id' => $fields[$val]['name_id'],
                                                                 'value' => $key
                                                                );
                        
                        }
                    }
                }            
            }
        
            if(isset($sett['ar_reviews_answers']) && $sett['ar_reviews_answers'] == 1) {           
            
                $comments = $this->reviews_model->getList('*,contact', array('where' => array('parent_id' => $reviews_ids)));
        
                foreach($comments as $comment) {                       
                    $reviews[$comment['parent_id']]['comments'][] = $comment;
                }
            }
        
            $this->view->assign('fields', $fields);
        } else {
            $reviews = array();
        }
        
        $this->view->assign('reviews', $reviews);
        
        $this->getResponse()->setTitle('Все отзывы');        
        $this->setLayout(new shopFrontendLayout());
        $this->setThemeTemplate('all-reviews.html');        
        
    }
}